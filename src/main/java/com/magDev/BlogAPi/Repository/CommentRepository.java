package com.magDev.BlogAPi.Repository;

import com.magDev.BlogAPi.Entity.Article;
import com.magDev.BlogAPi.Entity.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {

    List<Comment> findByArticle(Article article);
}