package com.magDev.BlogAPi.Repository;

import com.magDev.BlogAPi.Entity.Article;
import com.magDev.BlogAPi.Entity.Author;
import com.magDev.BlogAPi.Entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface ArticleRepository extends JpaRepository<Article, Long> {

        List<Article> findByAuthor(Author author);
        List<Article> findByCategories(Category category);
        }
