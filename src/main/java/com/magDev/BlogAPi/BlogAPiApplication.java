package com.magDev.BlogAPi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BlogAPiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BlogAPiApplication.class, args);
	}

}
