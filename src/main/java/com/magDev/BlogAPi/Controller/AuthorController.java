package com.magDev.BlogAPi.Controller;

import com.magDev.BlogAPi.Entity.Author;
import com.magDev.BlogAPi.Repository.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/authors")
public class AuthorController {

    @Autowired
    private AuthorRepository authorRepository;

    // list all authors
    @GetMapping
    @CrossOrigin(origins = "http://localhost:3000")
    public List<Author> getAllAuthors() {
        return authorRepository.findAll();
    }

    // create a new author
    @PostMapping
    public Author createAuthor(@RequestBody Author author) {
        return authorRepository.save(author);
    }

    // view a specific author
    @GetMapping("/{id}")
    public Author getAuthor(@PathVariable Long id) {
        return authorRepository.findById(id).orElse(null);
    }
}
