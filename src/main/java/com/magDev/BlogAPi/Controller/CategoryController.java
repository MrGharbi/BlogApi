package com.magDev.BlogAPi.Controller;

import com.magDev.BlogAPi.Entity.Category;
import com.magDev.BlogAPi.Repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/categories")
public class CategoryController {

    @Autowired
    private CategoryRepository categoryRepository;

    // list all categories
    @GetMapping
    @CrossOrigin(origins = "http://localhost:3000")
    public List<Category> getAllCategories() {
        return categoryRepository.findAll();
    }

    // create a new category
    @PostMapping
    public Category createCategory(@RequestBody Category category) {
        return categoryRepository.save(category);
    }

    // view a specific category
    @GetMapping("/{id}")
    public Category getCategory(@PathVariable Long id) {
        return categoryRepository.findById(id).orElse(null);
    }
}
