package com.magDev.BlogAPi.Data;

import com.magDev.BlogAPi.Entity.Article;
import com.magDev.BlogAPi.Entity.Author;
import com.magDev.BlogAPi.Entity.Category;
import com.magDev.BlogAPi.Entity.Comment;
import com.magDev.BlogAPi.Repository.ArticleRepository;
import com.magDev.BlogAPi.Repository.AuthorRepository;
import com.magDev.BlogAPi.Repository.CategoryRepository;
import com.magDev.BlogAPi.Repository.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;

@Configuration
public class DataInitialise implements CommandLineRunner {
    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    private AuthorRepository authorRepository;

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public void run(String... args) throws Exception {
        // create some test data
        Author author1 = new Author("John Smith");
        Author author2 = new Author("Jane Doe");
        Author author3 = new Author("Bob Johnson");
        authorRepository.saveAll(Arrays.asList(author1, author2, author3));

        Category category1 = new Category("Technology");
        Category category2 = new Category("Science");
        Category category3 = new Category("Sports");
        categoryRepository.saveAll(Arrays.asList(category1, category2, category3));

        Article article1 = new Article("Spring Boot Tutorial", "Learn how to use Spring Boot to build Java applications", author1, Arrays.asList(category1));
        Article article2 = new Article("Science of Sleep", "Understanding the importance of sleep and how it affects the brain", author2, Arrays.asList(category2));
        Article article3 = new Article("Soccer Training Tips", "Expert advice on how to improve your soccer skills", author3, Arrays.asList(category3));
        Article article4 = new Article("Java 8 Lambdas", "Learn about the new features in Java 8, including lambdas and the Stream API", author1, Arrays.asList(category1));
        Article article5 = new Article("The Benefits of Meditation", "Discover the many benefits of meditation and how to get started", author2, Arrays.asList(category2));
        Article article6 = new Article("Running for Beginners", "A guide to getting started with running, including tips on choosing the right shoes and creating a training plan", author3, Arrays.asList(category3));
        Article article7 = new Article("Git Basics", "Learn the fundamentals of version control with Git", author1, Arrays.asList(category1));
        Article article8 = new Article("The Science of Happiness", "Explore the latest research on happiness and learn simple ways to increase your own happiness", author2, Arrays.asList(category2));
        Article article9 = new Article("Baseball Training Tips", "Expert advice on how to improve your baseball skills", author3, Arrays.asList(category3));
        Article article10 = new Article("The Art of Negotiation", "Learn the skills and strategies to become a successful negotiator", author1, Arrays.asList(category1));
        articleRepository.saveAll(Arrays.asList(article1, article2, article3, article4, article5, article6, article7, article8, article9, article10));

        Comment comment1 = new Comment("Great tutorial!", article1);
        Comment comment2 = new Comment("I never realized how important sleep is!", article2);
        commentRepository.saveAll(Arrays.asList(comment1,comment2));
    }
}
