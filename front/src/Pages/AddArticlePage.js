import React, { useState,useEffect } from 'react';
import { message } from 'antd';

import AddArticleForm from '../Components/AddArticleForm';

const AddArticlePage = () => {
    const [authors, setAuthors] = useState([]);
    const [categories, setCategories] = useState([]);
    
    useEffect(() => {
      // fetch authors and categories from API and set them in state
      const fetchAuthors = async () => {
        const response = await fetch('http://localhost:8080/authors');
        const data = await response.json();
        setAuthors(data);
      };
      fetchAuthors();
      const fetchCategories = async () => {
        const response = await fetch('http://localhost:8080/categories');
        const data = await response.json();
        setCategories(data);
      };
      fetchCategories();
    }, []);
    
    const handleSubmit = async values => {
        console.log(JSON.stringify(values));
        const response = await fetch('http://localhost:8080/articles', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(values),
        });
    
        if (response.status === 201) {
          message.success('Article added successfully');
        } else {
          message.error('Error adding article');
        }
    };
    
    return (
      <div>
        <h1>Add Article</h1>
        <AddArticleForm
          authors={authors}
          categories={categories}
          onSubmit={handleSubmit}
        />
      </div>
    );
    };

export default AddArticlePage;