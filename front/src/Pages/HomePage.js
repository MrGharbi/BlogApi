import React, { useState, useEffect } from 'react';
import { Layout, Menu, Carousel, Row, Col } from 'antd';

const { Header, Content, Footer } = Layout;

const HomePage = () => {
  // State variable to store the articles list
  const [articles, setArticles] = useState([]);

  // Use the useEffect hook to fetch the articles list when the component mounts
  useEffect(() => {
    const fetchArticles = async () => {
      try {
        // Make an HTTP GET request to the server to fetch the articles list
        const response = await fetch('http://localhost:8080/articles');
        // Get the JSON data from the response
        const data = await response.json();
        // Update the state with the articles list
        setArticles(data);
        console.log(data);
      } catch (error) {
        console.error(error);
      }
    };
    fetchArticles();
  }, []);

  return (
    <Layout>
      {/* Use the Header component from Ant Design to display the navbar */}
      <Header>
        <div className="logo" />
        <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['1']}>
          <Menu.Item key="1">Home</Menu.Item>
          <Menu.Item key="2">About</Menu.Item>
          <Menu.Item key="3">Contact</Menu.Item>
        </Menu>
      </Header>
      <Content style={{ padding: '0 50px' }}>
        {/* Use the Carousel component from Ant Design to display the articles list as a carousel */}
        <Carousel autoplay>
          {articles.map(article => (
            <div key={article.id}>
              <h3>{article.title}</h3>
              <p>{article.content}</p>
            </div>
          ))}
        </Carousel>
        {/* Use the Row and Col components from Ant Design to display the articles list as a grid */}
        <Row gutter={16} style={{ marginTop: '20px' }}>
          {articles.map(article => (
            <Col key={article.id} span={8}>
              <div>
                <h3>{article.title}</h3>
                <p>{article.content}</p>
              </div>
            </Col>
          ))}
        </Row>
      </Content>
      <Footer style={{ textAlign: 'center' }}>Blog API ©2022 Created by OpenAI</Footer>
    </Layout>
  );
};

export default HomePage;
