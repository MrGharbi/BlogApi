import React, { useState } from 'react';
import { Form, Input, Button, Select, message } from 'antd';

const { Option } = Select;

const AddArticleForm = ({ authors, categories, onSubmit }) => {
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);

  const handleSubmit = async () => {
    try {
      setLoading(true);
      const values = await form.validateFields();
      // Modify the values object to send the author field in the desired format
      const modifiedValues = {
        ...values,
        author: {
          id: values.author
        },
        categories: values.categories.map(categoryId => ({
            id: categoryId
          }))
      };
      onSubmit(modifiedValues);
      form.resetFields();
      message.success('Article added successfully');
    } catch (error) {
      console.error(error);
      message.error('Error adding article');
    } finally {
      setLoading(false);
    }
  };

  return (
    <Form form={form} layout="vertical">
      <Form.Item
        name="title"
        label="Title"
        rules={[{ required: true, message: 'Please enter a title' }]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        name="content"
        label="Content"
        rules={[{ required: true, message: 'Please enter a content' }]}
      >
        <Input.TextArea rows={4} />
      </Form.Item>
      <Form.Item
        name="author"
        label="Author"
        rules={[{ required: true, message: 'Please select an author' }]}
      >
        <Select>
          {authors.map(author => (
          <Option key={author.id} value={author.id}>
          {author.name}
        </Option>
        ))}
      </Select>
    </Form.Item>
    <Form.Item
      name="categories"
      label="Categories"
      rules={[{ required: true, message: 'Please select at least one category' }]}
    >
      <Select mode="multiple">
        {categories.map(category => (
          <Option key={category.id} value={category.id}>
            {category.name}
          </Option>
        ))}
      </Select>
    </Form.Item>
    <Form.Item>
      <Button type="primary" onClick={handleSubmit} loading={loading}>
        Add Article
      </Button>
    </Form.Item>
  </Form>
);
};


export default AddArticleForm;